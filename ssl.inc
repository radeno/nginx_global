# https://cipherli.st
# https://www.owasp.org/index.php/TLS_Cipher_String_Cheat_Sheet
# https://www.acunetix.com/blog/articles/tls-ssl-cipher-hardening/
ssl_ciphers ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256;

ssl_protocols TLSv1.2 TLSv1.3;
ssl_prefer_server_ciphers on;
ssl_session_cache shared:SSL:20m;
ssl_session_timeout 120m;
ssl_session_tickets off;

ssl_dhparam /etc/nginx/ssl/dhparam.pem;

ssl_stapling on;
ssl_stapling_verify on;

#resolver 8.8.8.8 8.8.4.4 valid=300s;
#resolver_timeout 5s;

ssl_buffer_size 4k;
