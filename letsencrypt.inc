set $letsencrypt_pass_url http://letsencrypt:80;

location /.well-known/acme-challenge {
  proxy_pass $letsencrypt_pass_url;
  proxy_set_header X-Forwarded-For $letsencrypt_pass_url;
  proxy_set_header Host $host;
  proxy_redirect off;
}
